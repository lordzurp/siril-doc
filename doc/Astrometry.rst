Astrometry
==========

Astrometry is the science dealing with the positions and motions of celestial
objects. Astrometry is essential in modern astrophotography where capture
software like N.I.N.A, Ekos, APT or others, plate solve the images in 
order to obtain an astrometric solution, meaning they will precisely know the
position of the frame with regards to the sky. Astrometry can also be used at
processing stage, like in photometric color calibration tool for example.

.. toctree::
   :hidden:

   astrometry/platesolving
   astrometry/annotations




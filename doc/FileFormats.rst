
File Formats
############

There are several file formats that Siril can open and work on. However, only 
two are read natively and allow to build :ref:`sequences <sequences:sequences>`: 
the FITS and SER formats.

Here we will look at the different file formats read by Siril and understand 
the limitations of some and the strengths of others.


.. toctree::
   :hidden:

   
   file-formats/Bit-depth
   file-formats/Common
   file-formats/FITS
   file-formats/Astro-TIFF
   file-formats/SER


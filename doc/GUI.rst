Graphical User Interface
========================

The Graphical User Interface (GUI) allows you to process your images manually, 
as well as using scripts or typing commands. To learn how to use Siril in 
headless mode, please refer to this :ref:`section <Headless:Headless mode>`.

Siril's GUI is written using `GTK <https://www.gtk.org/>`_, a free and open 
source cross-platform toolkit for GUI creation. Currently, the version used is 
version 3.

The following subsections take you through the main interface window and 
useful menus.

.. toctree::
   :hidden:
   
   GUI/main-interface
   GUI/burger-menu
   GUI/shortcuts
   GUI/information_window

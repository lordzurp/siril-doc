Main interface
==============

When launching Siril, the main interface opens.

.. note::
   Click anywhere onto the image below to display its functions.

.. _main-interface_window:
.. image:: ../_images/GUI/main-interface_window.svg
   :alt: Main interface
   :width: 100% 

.. _window-image-area:

.. rubric:: Image area

This area displays the currently loaded image. Click on :guilabel:`Red`, 
:guilabel:`Green` or :guilabel:`Blue` to switch between the different layers 
(color images only, a single :guilabel:`B&W` tab is available for mono images).

Right-click on the image to display a contextual menu:

.. image:: ../_images/GUI/main-interface_image-area_right-click.png
   :alt: Right-click

.. tip::
   When no image is loaded, double-clicking on the image area pops the Open dialog.


.. todo::
   Explain or link (when created) to the different items

:ref:`Back to figure <main-interface_window>`

.. _window-open:

.. rubric:: Open

Click on these icons (from left to right) to:

* open a file
* open a recent file
* change the `Home` directory

:ref:`Back to figure <main-interface_window>`

.. _window-livestack:

.. rubric:: Livestack

Click on this button to start a :doc:`../Livestack` session.

:ref:`Back to figure <main-interface_window>`

.. _window-undo:

.. rubric:: Undo/Redo

Use these buttons to undo/redo last actions. This is only available if last 
action has been done through GUI, not by typing a command.

:ref:`Back to figure <main-interface_window>`

.. _window-image-processing:

.. rubric:: Image processing

Click on this button to display the :ref:`Processing <processing:processing>` 
menu.

:ref:`Back to figure <main-interface_window>`

.. _window-scripts:

.. rubric:: Scripts

Click on this button to display and launch the :ref:`scripts <scripts:scripts>`.

:ref:`Back to figure <main-interface_window>`

.. _window-info-bar:

.. rubric:: Information bar

This bar displays the current version of Siril and the path to the current
working directory. 

* To the right, informationabout available RAM and disk space id also given.
* You can change the available number of threads used by Siril using the +/- 
  signs.

:ref:`Back to figure <main-interface_window>`

.. _window-save:

.. rubric:: Save

These buttons are used to save your results:

* save (overwrites) the current image.
* save with a different name and/or extension.

  .. figure:: ../_images/GUI/save_dialog.png
     :alt: starnet dialog
     :class: with-shadow
     :width: 100%

     Save dialog box.
     
  The drop-down list at the bottom right allows you to choose the type of image
  recorded. It automatically adds the extension to the file name. However, by 
  staying in the :guilabel:`Supported Image Files` mode, it is possible to add 
  any extension supported by Siril by hand and it will save in the correct file
  format.
   
* take a snapshot of the current view (as seen on the screen, meaning preview 
  strecthing, if any, is applied). There are two possible options. Either the 
  snapshot is saved in the clipboard, or directly copied to the disk in the 
  working directory.
* change the bitdepth of the current image. The choice is between 16-bit and 
  32-bit.

:ref:`Back to figure <main-interface_window>`

.. _window-burger-menu:

.. rubric:: Burger menu

Opens the main menu, also called :ref:`burger menu <gui/burger-menu:burger menu>`. Gives access
to :doc:`Preferences <../preferences/preferences_gui>`, :doc:`platesolving <../astrometry/platesolving>` 
and much more.

:ref:`Back to figure <main-interface_window>`

.. _window-tabs:

.. rubric:: Tabs

Selects one of the tabs. You can also switch between the different tabs using 
:kbd:`F1` to :kbd:`F7` shortcuts.

More details can be found there:

- :ref:`Conversion <preprocessing/conversion:conversion>` tab
- :ref:`Sequence <sequences:loading a sequence>` tab
- :ref:`Calibration <preprocessing/calibration:calibration>` tab
- :ref:`Registration <preprocessing/registration:registration>` tab
- :ref:`Plot <plot:plotting feature>` tab
- :ref:`Stacking <preprocessing/stacking:stacking>` tab

:ref:`Back to figure <main-interface_window>`

.. _window-tab-window:

.. rubric:: Tab window

Displays the specifics of the currently selected tab.

:ref:`Back to figure <main-interface_window>`

.. _window-command-line:

.. rubric:: Command line

Type in a `command <https://free-astro.org/index.php/Siril:Commands>`_ and 
press :kbd:`Enter`.

* You can press the button at the end of the line to get some help on the 
  usage.
* You can also abort the process being currently executed by clicking on the
  :guilabel:`Stop` button.

:ref:`Back to figure <main-interface_window>`

.. _window-expand:

.. rubric:: Expand

Click on this bar to expand/retract the whole tab/tab window area.

:ref:`Back to figure <main-interface_window>`

.. _window-image-sliders:

.. rubric:: Image sliders

Use the top and bottom sliders to adjust the white and black points of the 
previewed image (in Linear mode).

.. tip::
   Click on the name of the `Image` or `Sequence` loaded to copy its name to 
   the clipboard (useful to paste in a command).


:ref:`Back to figure <main-interface_window>`

.. _window-preview:

.. rubric:: Preview mode

Select the preview mode for the loaded image, between the following choices:

* Linear
* Logarithm
* Square root
* Squared
* Asinh
* Autostretch (tick the High Definition box to use a deeper (up to
  24-bit, configurable in Preferences) LUT instead of the default 16-bit one)
* Histogram

In Autostretch mode with color images, the toggle to the right 
activates/deactivates channel linking. When unlinked, the 3 layers are 
stretched independantly so as to give a more balanced image.

.. warning::
   This is just a preview of the image, not the actual data (except if Linear 
   mode is selected). Do not forget to stretch your images before saving them.

:ref:`Back to figure <main-interface_window>`

.. _window-special-views:

.. rubric:: Special views

Use these toggles to show previewed images:

* in inverted colors
* in false colors

:ref:`Back to figure <main-interface_window>`

.. _window-astrometry-tools:

.. rubric:: Astrometry tools

Use these toggles to show:

* astrometric :ref:`annotations <astrometry/annotations:annotations>`
* celestial grid

.. warning::
   The loaded image needs to be plate-solved for these buttons to be active.

:ref:`Back to figure <main-interface_window>`

.. _window-quick-photometry:

.. rubric:: Quick photometry

Use this toggle to trigger the 
:ref:`quick photometry <photometry/quickphotometry:quick photometry>` mode.

:ref:`Back to figure <main-interface_window>`

.. _window-intensity-profile:

.. rubric:: Intensity profile

Use this toggle to trigger the
:ref:`intensity profile <intensity-profiling>` mode.

:ref:`Back to figure <main-interface_window>`


.. _window-zoom:

.. rubric:: Zoom

Use these buttons to:

* Zoom out
* Zoom in
* Zoom to fit the avaialble window space
* Zoom to actual size

.. tip::
   :kbd:`Ctrl+left clic` will allow to navigate into the picture

.. tip::
   :kbd:`Ctrl+mouse scroll` will zoom in/out and :kbd:`Ctrl` + :kbd:`0` / :kbd:`1` 
   will zoom to fit/100%.

.. tip::
   :kbd:`Ctrl+Shift` and drag with the primary mouse button will measure the distance between two points. If sufficient metadata is available the measurement
   will be given in degrees, minutes and arcseconds, otherwise it will be given in
   pixels.

:ref:`Back to figure <main-interface_window>`

.. _window-geometric-transformations:

.. rubric:: Geometric transformations

Use these buttons to:

* Rotate left
* Rotate right
* Mirror about horizontal axis
* Mirror about vertical axis

:ref:`Back to figure <main-interface_window>`

.. _window-frame-selection:

.. rubric:: Frame selection

Click on this button to open the :ref:`frame selector <sequences:frame selector>`.

:ref:`Back to figure <main-interface_window>`

Shortcuts
=========

.. |burger-menu| image:: ../_images/preferences/burger.png

Siril uses several shortcuts to access processing tools or to manipulate the 
application and/or the images. These shortcuts are all detailed in the 
:guilabel:`Keyboard Shortcuts` dialog accessible via the burger menu |burger-menu|.

.. image:: ../_images/GUI/shortcuts_1.png
   :alt: Shortcuts Page 1


.. image:: ../_images/GUI/shortcuts_2.png
   :alt: Shortcuts Page 2

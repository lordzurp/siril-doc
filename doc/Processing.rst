Processing
==========

This section takes you through the different processing steps of your images. 
The drop-down menu is accessible from the header bar using the 
:guilabel:`Image Processing` button. The tools are grouped in the menu, and in 
this documentation too, by theme.

.. figure:: _images/processing/processing_menu.png
    :alt: processing menu
    :class: with-shadow

    Image processing menu

.. toctree::
   :hidden:

   processing/stretching
   processing/colors
   processing/filters
   processing/stars
   processing/geometry
   processing/background
   processing/extraction
   processing/lmatch
   processing/rgbcomp
   processing/merge
   processing/pixelmath



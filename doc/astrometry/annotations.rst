###########
Annotations
###########

Annotations are glyphs displayed on top of images to depict the presence
of known sky objects, like galaxies, bright stars and so on. They come
from catalogues but can only be displayed on images for which we know
which part of the sky they represent, images that have been **plate
solved** and contain the world coordinate system (WCS) information in
their header, so only FITS or Astro-TIFF files.

.. figure:: ../_images/annotations/example.png
   :alt: example
   :class: with-shadow
   :width: 100%

   View of full annotated image

Plate solving, can be done within Siril in the 
:menuselection:`Image Information --> Image Plate Solver...` entry, or using 
external tools like
`astrometry.net <https://nova.astrometry.net/>`__ or
`ASTAP <https://www.hnsky.org/astap.htm>`__.

.. figure:: ../_images/annotations/plate_solved_gui.png
   :alt: plate solved GUI
   :class: with-shadow
   :width: 100%

   Buttons for annotations

When a plate solved image is loaded in Siril, you can see the sky
coordinates for the pixel under the mouse pointer displayed at the
bottom right corner and the buttons related to annotations become
available. The first button toggles on or off object annotations,
the second the celestial grid and the compass.

*******************
Types of catalogues
*******************

Siril comes with a predefined list of catalogues for annotations: 

* Messier catalogue (M) 

* New General Catalogue (NGC) 

* Index Catalogue (IC) 

* Lynds Catalogue of Dark Nebulae (LdN) 

* Sharpless Catalogue (Sh2)

* Star Catalogue (3661 of the brightest stars)

In addition, 2 *user defined catalogues* can be used: 

* User DeepSky Objects Catalogue 

* User Solar System Objects Catalogue

********************
Catalogue management
********************

Both these catalogues can be enabled/disabled for display in the 
:menuselection:`Preferences menu --> Astrometry` :ref:`tab <preferences/preferences_gui:astrometry>`.

A slider on the right side, allows you to easily navigate across the
catalogue list.

.. figure:: ../_images/annotations/cat-manage_view.png
   :alt: Catalogue management
   :class: with-shadow
   :width: 100%

   Catalogue management in Preferences/Astrometry

The two *user defined catalogues* can also be purged (ie deleted) via
the appropriate buttons.

The *user catalogues* (DSO and SSO) are stored in the user settings directory
and can be easily modified.

Their location depends on the operating system: 

* for *Unix-based* OS they will be in ``~/.config/siril/catalogue`` 

* on *Windows* they are in ``%LOCALAPPDATA%\siril\catalogue``.

The position of the compass on the image can be adjusted from the
preferences too.

.. _Local_Catalog:

These annotation catalogues are for display purposes only. They are not
used in astrometry or photometry tools, contrary to the star catalogues
like NOMAD, which can now be `installed
locally <https://free-astro.org/index.php?title=Siril:Star_Catalogues>`__
too.

.. figure:: ../_images/annotations/bottom.png
   :alt: Bottom
   :class: with-shadow

   Local Catalogue (NOMAD) setup

**************************
Searching for a new object
**************************

When the name of an object in the image is known (if not, see the 
`Inverse Search`_ section), it is possible to add it to annotations:

* with the image loaded and plate solved, type :kbd:`Ctrl` + :kbd:`Shift` + 
  :kbd:`/` or :guilabel:`Search` in the pop-up menu (right click).

A small search dialog will appear. In it, object names can be entered,
then pressing :guilabel:`Enter` will first search it in the existing annotation
catalogues in case it already exists under another name. If not it will send an
online request to SIMBAD (for a star of Deep Sky Object) to get the coordinates
of an object with such a name. If found, and not already in any catalogue, the
object will be added to the *Deep Sky user catalogue*.

The items of this catalogue are displayed in *ORANGE* while the objects
from the predefined catalogues are displayed in *GREEN*.

.. figure:: ../_images/annotations/user_dso.png
   :alt: User DSO
   :class: with-shadow
   :width: 100%

   Deep sky objects from user and predefined catalogues

From Siril version 1.2, we can now search for solar system objects too,
using the `Miriade
ephemcc <https://ssp.imcce.fr/webservices/miriade/api/ephemcc/>`__
service. This is done by prefixing the name of the object to be searched
by some keyword representing the type of object: ``a:`` for asteroids,
``c:`` for comets, ``p:`` for planets. Since they are moving objects,
they can be added several times, and the request is done for the date of
observation of the currently loaded image. The date is associated to the
name in the *Solar System user catalogue*. The items of this catalogue
are displayed in *YELLOW*.

Examples of valid inputs (not case sensitive): 

* ``HD 86574`` or ``HD86574`` are both valid for this star 

* ``c:67p`` or ``c:C/2017 T2`` are valid forms for comets 

* ``a:1`` and ``a:ceres`` are both valid for *(1) Ceres* 

* ``a:2000 BY4`` is valid for *103516 2000 BY4* 

* ``p:4`` or ``p:mars`` for Mars

******************************************************************
Filling a Solar System user Catalogue: which SSO is in this field?
******************************************************************

To answer the question *Is there any known solar system object in my image?*, a
special function does a request to an online server of the IMCCE too (`SkyBoT
<https://ssp.imcce.fr/webservices/skybot/api/conesearch/>`__) and displays the
results in the console and in the image.

* with the image loaded and plate solved, right click/``Solar System Objects``,
  or in the command line you can use the ``solsys`` function.

It displays in *RED* all the Solar System objects in the field of view
(if any are known and found of course). Objects magnitudes and equatorial
coordinates for the image date are printed in the console.

These red annotations will be erased as soon as the ``Show Objects names``
button is toggled.

.. figure:: ../_images/annotations/solsys.png
   :alt: Solar Sytem Result
   :class: with-shadow
   :width: 100%

   Result of a Search Solar System process

However, you may want to save any particular item in the User Solar System
Objects Catalogue. It can be done by using the ``Search`` command for a solar
system object as previously described.

This way, the saved item is diplayed in *YELLOW* and will be displayed in any
image that has this field of view by enabling the annotations.

.. figure:: ../_images/annotations/solsys_and_search.png
   :alt: Solar System
   :class: with-shadow
   :width: 100%

   View with predefined/DSO/SSO

.. note::
   Newly discovered objects, or some fast moving objects, will have their
   position misaligned with the image. This is often the case for comets for
   example, which can be an arcminute off. This happens because the orbital
   parameters of the object are not very well known or that they have not been
   updated recently in the system. If you are looking for an alternate
   computation of the coordinates of the known objects of the field, you might
   query manually the `JPL Small Body identification tool
   <https://ssd.jpl.nasa.gov/tools/sb_ident.html#/>`_.

.. _Inverse Search:

****************************************
The inverse search: what is this object?
****************************************

Especially useful for photometry works, it is possible to identify a
star or other objects in the image by drawing a selection around them,
right clicking to bring up the context menu, and selecting the :guilabel:`PSF`
entry. This will open the PSF window, and if it’s a star it will display
the Gaussian fit parameters, but it will also display a Web link at the
bottom left of the window: opening it will bring you to the `SIMBAD
page <https://simbad.cds.unistra.fr/simbad/sim-fcoo>`__ for the
coordinates of the object and in many cases will give you the name of
the object. SIMBAD doesn’t have all known objects, but the coordinates
from the page can still be used as a starting point to look for the
object in other online catalogues, for example `Gaia DR3
(VizieR) <https://vizier.cds.unistra.fr/viz-bin/VizieR-3?-source=I/355/gaiadr3>`__.

****************
Extra catalogues
****************

Sometimes, users create their own catalogues, we can try to link them
here to help everybody. They are *user catalogues*, so installing them
requires either replacing the current user catalogue, or by manually
merging their lines into a new file.

List of known user catalogues: 

* Variable stars, extracted from `GCVS <http://www.sai.msu.su/gcvs/gcvs/intr.htm>`__ 5.1, discussed `here
  in French <https://www.webastro.net/forums/topic/196778-annotation-sous-siril-avec-catalogue-utilisateur/#comment-2941770>`__,
  (:download:`file link <GCVS_siril_annotations.txt>`).


Platesolving
############

The platesolving is a major step in astronomical image processing. It allows
images to be associated with celestial coordinates, giving the ability to know
what object is in the observed field of view. Many of Siril's tools, such as
the Photometric Color Calibration (PCC) tool, need to know the coordinates of
the image with sufficient accuracy in order to work.

Astrometry in Siril can be performed in a few different ways:

* Using the dedicated tool accessible via the
  :menuselection:`burger menu --> Image Information --> Image Plate Solver`, or
  using the shortcut :kbd:`Ctrl` + :kbd:`Shift` + :kbd:`A`.

.. figure:: ../_images/platesolver/Astrometry_Dialog.png
   :alt: Astrometry dialog
   :class: with-shadow

   Platesolving dialog

* Using the :ref:`photometric color calibration <processing/colors:Photometric Color Calibration>`
  tool, based on the same tool but extended to add star color analysis and
  comparison with star colors in catalogs to adjust the image's color,
  available in the :menuselection:`Image Processing menu --> Color Calibration
  --> Photometric Color Calibration` or using the shortcut :kbd:`Ctrl` +
  :kbd:`Shift` + :kbd:`P`.

.. figure:: ../_images/processing/color_pcc_dialog.png
   :alt: Astrometry dialog
   :class: with-shadow

   Photometric Color Calibration tool

* Using the ``platesolve`` command, introduced in Siril 1.2.

Since version 1.2, plate solving can be done by two different algorithms. The
first was the only one in Siril until this version, it's based on the global
registration's star matching algorithm, trying to register images onto a
virtual image of a catalog with the same field of view. The second is new, it
is using an external program called ``solve-field`` from the Astrometry.net
suite, installed locally. For Windows platforms, the simplest way to get it is
to use `ansvr <https://adgsoftware.com/ansvr/>`_.

Astrometric solutions require a few parameters to be found, like image sampling.
The window of the tool helps gathering those parameters, we will now see how to
fill them correctly.

Image parameters
================

Target coordinates
``````````````````
Finding an astrometric solution is easier and faster when we roughly know were
we are looking. Siril's plate solver, as it's comparing a catalog with the
image, needs to know approximately the position of the center of the image to
get the catalog excerpt. Astrometry.net has all the catalogs it needs locally,
so it can browse through all of it to find a solution, but it is of course much
faster to tell it where to start.

Acquisition software often also control the telescope nowadays and should know
the approximate coordinates where the image was taken. In that case, using a
FITS format, these coordinates will be provided in the image metadata, the FITS
header. This is not always the case, and clearly not the case when RAW DSLR
images are created instead of FITS.

When opening the plate solver or PCC windows, the current image's metadata is
loaded and displayed in the window. If no coordinates appear at the top, or if
RA and Dec remain zero, some user input is needed. If you don't know at all
what the image is, use a blind solve with astrometry.net. Otherwise, provide
equatorial J2000 coordinates corresponding to as close as the center of the
image as possible, either by filling the fields if you already know the
coordinates, or by making a query with an object name (not yet possible from
the command).

The text field at the top left of the window is the search field, its goal
being to convert an object name to its coordinates. Pressing :kbd:`ENTER` or
clicking the :guilabel:`Find` button will first search the object in the local
annotation catalogues. If not found, a Web request will be made to obtain its
coordinates. Several results may be found for the entered name, they will be
displayed in the list below. Selecting one updates the coordinates at the top,
the first is selected by default.

It is also possible to choose the server on which you want to execute the
query, it does not change the results much, but sometimes one of them can be
online, so others would act as a backup, between CDS, VizieR and SIMBAD
(default).

.. note::
   If the object is not found, please try with the full name or with the name
   from a catalogue. The annotation catalogues contain a few common names, the
   online services too, but not all, and they don't find partial answers.
   For example, for the Bubble Nebula, please enter ``NGC 7635`` or ``bubble
   nebula``, not ``bubble``.

The coordinate fields are filled in automatically, but it is possible to define
your own. Don't forget to check the `S` box if the object you are looking for
is located in the southern hemisphere of the sky (negative declinations).

Image sampling
``````````````
Image sampling is the most important parameter for plate solving. Given in
arcseconds per pixel in our case, it represents how much zoomed on the sky the
image is, so how wide a field to search for.

It is derived from two parameters: focal length and pixel size. They are often
available in the image metadata as well. When not available from the image, the
values stored in the settings are used. The values of the images and of the
preferences can be set using the :guilabel:`Information` dialog. In any case,
check the displayed value before plate solving and correct if needed. If an
astrometric solution is found, the default focal length and pixel size will be
overwritten. This behavior can be disabled in the settings.

.. warning::
   If binning was used, it should be specified in the FITS header, but this can
   take two forms: the pixel size can remain the same and the binning
   multiplier should be used to compute the sampling, or the pixel size is
   already multiplied by the acquisition software. Depending on the case you
   are facing, either of the forms can be chosen from the preferences or from
   the :guilabel:`Information` window.

Pixel size is given in the specification of astronomical cameras, and can
generally be found on the Web for DSLR or other cameras. The number of sensors
is limited and most of them are known.

Focal length depends on the main instrument, but also on backfocus and
correcting or zooming lenses used. Give a value as close as what you believe
the effective focal to be, if an astrometric solution is found, the computed
focal length will be given in the results and you will be able to reuse that in
your acquisition software and for future uses of the tool.

When either of the fields is updated, the sampling is recomputed and displayed
in the window (called 'resolution' here). Make sure the value is as close as
reality as possible.

Other parameters
````````````````
Finally, there are three toggle buttons at the bottom of the frame:

#. The option :guilabel:`Downsample image` downsamples the input image to speed-up
   star detection in it. The downside is that it may not find enough stars or
   give a less accurate astrometric solution. The size of the output image
   remains unchanged.

#. If the image is detected as being upside-down by the astrometric solution, 
   with the option :guilabel:`Flip image if needed` enabled, it will be flipped at
   the end. This can be useful depending on the capture software, if the image
   has not the right orientation when it is displayed in Siril (see more
   `explanations <https://free-astro.org/index.php?title=Siril:FITS_orientation>`_).

#. When the option :guilabel:`Auto-crop (for wide field)` is applied, it performs a
   platesolve only in the center of the image. This is only done with wide
   field images (larger than 5 degrees) where distortions away from the center
   are important enough to fool the tool. Ignored for astrometry.net solves.

Catalogue parameters
====================

By default, this section is insensitive because everything is set to automatic.
By unchecking the auto box, however, it is possible to choose the online
catalog used for the platesolve, which may depend on the resolution of the
image. The choice is done between:

* **TYCHO2**, a catalogue containing positions, proper motions, and two-color
  photometric data for 2,539,913 of the brightest stars in the Milky Way.

* **NOMAD**, a simple merge of data from the Hipparcos, Tycho-2, UCAC2,
  Yellow-Blue 6, and USNO-B catalogs for astrometry and optical photometry,
  supplemented by 2MASS near-infrared. The almost 100 GB dataset contains
  astrometric and photometric data for about 1.1 billion stars.

* **Gaia DR3**, released on 13 June 2022. The five-parameter astrometric
  solution, positions on the sky (α, δ), parallaxes, and proper motions, are
  given for around 1.46 billion sources, with a limiting magnitude of G = 21.

* **PPMXL**, a catalog of positions, proper motions, 2MASS- and optical
  photometry of 900 million stars and galaxies.

* **Bright Stars**, a star catalogue that lists all stars of stellar magnitude
  6.5 or brighter, which is roughly every star visible to the naked eye from
  Earth. The catalog contains 9,110 objects.

.. note::
   An internet connection is required to use these online catalogs.

The :guilabel:`Catalogue Limit Mag` is an option that allows you to limit the 
magnitude of the stars retrieved in the catalog. The automatic value is 
calculated from the image resolution.

Using local catalogues
``````````````````````

With version 1.1, starting in June of 2022, it was possible to rely on a
locally installed star catalogue, for disconnected or more resilient operation.
The star catalogue we found to be the most adapted to our needs is the one from
`KStars <https://edu.kde.org/kstars/>`_. It is in fact composed of four
catalogues (`documented here in KStars <https://api.kde.org/kstars/html/Stars.html>`_),
two of them not being directly distributed in the base KStars installation files:

* **namedstars.dat**, the brightest stars, all of them have names

* **unnamedstars.dat**, also bright stars, but down to magnitude 8

* **deepstars.dat**, fainter stars extracted from The Tycho-2 Catalogue of the
  2.5 Million Brightest Stars, down to magnitude 12.5

* **USNO-NOMAD-1e8.dat**, an extract of the huge NOMAD catalogue limited to B-V
  photometric information and star proper motion in a compact binary form, down
  to magnitude 18.

When comparing these catalogues with the online NOMAD, we can easily see that
many stars are missing. If not enough are found for your narrow field, you
should still use the remote queries. A nice thing to check when the catalogues
are installed is highlighting which stars of the image will be used for the
PCC, those available with photometric information in the catalogues, using the
:ref:`nomad <nomad>` command.

Download
''''''''

The first two files are available in
`KStars source <https://invent.kde.org/education/kstars/-/tree/master/kstars/data>`_,
the Tycho-2 catalogue from a `debian package <https://tracker.debian.org/pkg/kstars-data-extra-tycho2>`_
and the NOMAD catalogue from KStars files too, as documented in this small
`article for KStars installation <http://knro.blogspot.com/2017/03/how-to-get-100-million-stars-in-kstars.html>`_.
It is has multiple worldwide `mirrors <https://files.kde.org/edu/kstars/download.kde.org/kstars/USNO-NOMAD-1e8-1.0.tar.gz.mirrorlist>`_
as indicated in the articles.

To make things easier to Siril users, and possibly to KStars users too, we
redistribute the four files in a single place, and in a more compressed format.
With the LZMA algorithm (used by xz or 7zip), the file size is 1.0GB instead of
the 1.4GB with the original gzip file.

To make it available from anywhere faster, it is distributed with bittorrent,
using this `torrent file <https://free-astro.org/download/kstars-siril-catalogues.torrent>`_
or the following `magnet link <magnet:?xt=urn:btih:0d0d891df2779bcfc691638d216551bf9e44cb59&dn=kstars-siril-catalogues&tr=http%3A%2F%2Ffree-astro.org%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80%2Fannounce&tr=udp%3A%2F%2Fbt1.archive.org%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce>`_.

Slower direct download links are available `here <https://free-astro.org/download/kstars-siril-catalogues/>`__ 
(right click on each file name on the left and save the links).

Installation in Siril
'''''''''''''''''''''

The files can be put anywhere and their paths given to Siril in the settings,
but there is a default location for the four files: ``~/.local/share/kstars/``
on Linux. They can be linked there to avoid unnecessary copies. Settings can be
changed from the command line now, using the :ref:`set <set>` command.

When available and readable, Siril will not use the Web service to retrieve
astrometric or photometric data. See the messages in the log tab or on the
console to verify that the catalogue files are used as expected.

Only **SIMBAD** will be used to convert object names into coordinates if
required, but that should only be needed if the acquisition software did not
record the target coordinates in the FITS header, or when using SER file format
which cannot hold this information.

Usage
'''''

With the addition of the new link between Siril's plate solver and the local
catalogue and the new link between Siril's PCC and the local catalogue, a new
command nomad was created to display which stars in a plate solved image
contain photometric information (the B-V index) and can be used for
calibration.

This is a good way to verify that the plate solving and the image are aligned,
in addition to the object annotation feature (see :ref:`annotations
<astrometry/annotations:annotations>`).

.. figure:: ../_images/platesolver/Local_Catalogue.png
   :alt: Preferences with local catalogues
   :class: with-shadow
   :width: 100%

   Preferences with local catalogues

Technical information
'''''''''''''''''''''

For photometry, Siril only uses the `B-V index <https://en.wikipedia.org/wiki/Color_index>`_,
which gives information about star colour. The three image channels are then
scaled to give the best colour representation to all stars in the image.

For more information about the KStar binary file type, see `this page <https://api.kde.org/kstars/html/Stars.html>`_
and this discussion on `kstars-devel <https://mail.kde.org/pipermail/kstars-devel/2022-June/007521.html>`_
and some development notes in Siril `here <https://gitlab.com/free-astro/siril/-/blob/829_local_nomad/subprojects/htmesh/README>`_
and `here <https://gitlab.com/free-astro/siril/-/blob/829_local_nomad/src/io/catalogues.c>`__.

Sha1 sums for the 4 catalogue files::

    4642698f4b7b5ea3bd3e9edc7c4df2e6ce9c9f7d  namedstars.dat
    53a336a41f0f3949120e9662a465b60160c9d0f7  unnamedstars.dat
    d32b78fd1a3f977fa853d829fc44ee0014c2ab53  deepstars.dat
    12e663e04cae9e43fc4de62d6eb2c69905ea513f  USNO-NOMAD-1e8.dat

`Licenses <https://gitlab.com/free-astro/siril/-/blob/master/src/io/kstars/KStars_catalogues_LICENSES.txt>`_
for the 4 catalogue files.

Using the local astrometry.net solver
=====================================

Since version 1.2, ``solve-field``, the solver from the astrometry.net suite,
can be used by Siril to plate solve images or sequence of images.

For Windows platforms, the simplest way to get it is to use `ansvr
<https://adgsoftware.com/ansvr/>`_. If you did not modify the default installation 
directory, that is `%LOCALAPPDATA%\\cygwin_ansvr`, Siril will search for it 
without additional setup. If you have cygwin and have build astrometry.net
from the sources, you must specify the location of cygwin root in the 
:ref:`Preferences <Preferences/preferences_gui:astrometry>`.

For other OS, the executable is expected to be found in the
``PATH``.

The use of this tool makes it possible to *blindly* solve images, without a
priori knowledge of the area of the sky they contain. It's also a good
alternative to Siril's plate solver in case it fails, because it's a dedicated 
and proven tool that also can take field distorsion into account.

Default settings should be fine, but can be modified if you really want to,
using the :ref:`set <set>` command (default values specified between parens) or
in the :ref:`Astrometry <Preferences/preferences_gui:Astrometry>` tab of 
preferences. How wide the range of allowed scales is (15%), how big the radius 
of the search from initial coordinates is (10 degrees), the polynomial order 
for field distorsion (0, disabled), removing or not the temporary files (yes), 
using the result as new default focal length and pixel sizes (yes).

Index files
```````````

Astrometry.net needs index files to run. We strongly recommend you use the latest
index files available from their `website <http://data.astrometry.net/>`_, i.e. 
the 4100 and 5200 series. The field of view of each series is described in their
`github page <https://github.com/dstndstn/astrometry.net/blob/main/doc/readme.rst>`_.
(the official documentation does not yet include this table).

On Unix-based system, you can just follow along the instructions in the documentation.

On Windows, if you are running ansvr, those recent index files will not be made 
available by the Index Downloader. You can still download them separately and 
store them where the other index files are kept (would recommend 
to remove the old files, although it may mess up the Index Downloader).

How it works
````````````

Just like the internal solver, Siril will proceed with extracting the stars from 
your images (so as to benefit from internal parallelism) and submit this list of stars 
to astrometry.net ``solve-field``. If you then want astrometry.net to crawl the 
index in parallel, you will need to specify it through the astrometry.cfg file.

Star detection
==============

By default, the star detection uses the *findstar* algorithm with the current
settings. It works very well to find many stars, but in some occasions we would
like to detect the stars manually, or simply view which are used. A first step
would be to open the :guilabel:`PSF` window and launch star detection, then
adjust the settings (see the related documentation :ref:`documentation
<Dynamic-PSF:Use>`).

Another approach would be to select the stars one by one by surrounding them
with a selection then via a right click, choose :guilabel:`Pick a Star`. The 
more stars selected, the more likely the algorithm is to succeed.

Then in the astrometry window, expand the star detection section and activate
the :guilabel:`Manual detection`. Instead of running *findstar*, it will use the
current list of stars.

Understanding the results
=========================

When an astrometric solution is found, we can see in the Console tab this kind of messages:

.. code-block:: text

        232 pair matches.
        Inliers:         0.996
        Resolution:      0.196 arcsec/px
        Rotation:     -115.21 deg (flipped)
        Focal length: 3959.95 mm
        Pixel size:      3.76 µm
        Field of view:    31' 15.46" x 20' 51.09"
        Saved focal length 3959.95 and pixel size 3.76 as default values
        Image center: alpha: 21h32m41s, delta: +57°36'22"
        Flipping image and updating astrometry data.

The astrometric solution gives us the J2000 equatorial coordinates of the image
center, the projected horizontal and vertical dimension of the image on the
sky, the focal length that could give this field for the given pixel size and
consequently the actual image sampling, the angle the image makes with the
north axis and some information about how many stars could be used to achieve
the solution.

If it fails, check that start coordinates and pixel size are correct and try
changing the input focal length from a factor 2, this will change the amount of
stars downloaded from the catalogs, and maybe more stars will be identified. If
Siril's plate solve won't find a solution, it is still possible to use an
external tool to do it, the solution will be written in the FITS header either
way.


| Converts all images in a known format into Siril's FITS images.
| 
| The argument **basename** is the basename of the new sequence. For FITS images, Siril will try to make a symbolic link. If not possible, files will be copied.
| The flags **-fitseq** and **-ser** can be used to specify an alternative output format, other than the default FITS.
| The option **-debayer** applies demosaicing to images. In this case no symbolic link are done.
| **-start=index** sets the starting index number and the **-out=** option converts files into the directory **out**

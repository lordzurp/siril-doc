| Crops to a selected area of the loaded image.
| 
| If a selection is active, no further arguments are required. Otherwise, or in scripts, arguments have to be given, with **x** and **y** being the coordinates of the top left corner, and **width** and **height** the size of the selection. Alternatively, the selection can be made using the BOXSELECT command
| 
| Links: :ref:`boxselect <boxselect>`

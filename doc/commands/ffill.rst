| Same command as FILL but this is a symmetric fill of a region defined by the mouse. Used to process an image in the Fourier (FFT) domain
| 
| Links: :ref:`fill <fill>`

| Fills the whole current image (or selection) with pixels having the **value** intensity expressed in ADU

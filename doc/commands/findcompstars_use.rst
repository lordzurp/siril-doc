.. code-block:: text

    findcompstars star_name [-narrow|-wide] [-catalog={nomad|apass|}] [-dvmag=3] [-dbv=0.5] [-out=nina_file.csv]
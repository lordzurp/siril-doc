| Subtracts the image in memory by the image **filename** given in argument.
| Result will be in 32 bits per channel if allowed in the preferences, so capable of storing negative values. To clip negative value, use 16 bit input images or clip use the THRESHLO command
| 
| Links: :ref:`threshlo <threshlo>`

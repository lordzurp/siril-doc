| Links all FITS images in the working directory with the **basename** given in argument.
| 
| If no symbolic links could be created, files are copied. It is possible to convert files in another directory with the **-out=** option

| Process the provided image for live stacking. Only possible after START_LS
| 
| Links: :ref:`start_ls <start_ls>`

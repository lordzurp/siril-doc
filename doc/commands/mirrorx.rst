| Flips the image about the horizontal axis. Option **-bottomup** will only flip it if it's not already bottom-up

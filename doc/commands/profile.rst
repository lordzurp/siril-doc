| Generates an intensity profile plot between 2 points in the image. The arguments may be provided in any order. The arguments **-to=x,y** and **-from=x,y** are mandatory.
| 
| The argument **-layer=\ {red \| green \| blue \| lum \| col}** specifies which channel (or luminance or colour) to plot if the image is color. This can be used in conjunction with spectrometric options. It may also be used with the **-tri** option, which generates 3 parallel equispaced profiles each separated by **-spacing=** pixels, but note that for tri profiles the **col** option will be treated the same as **lum**.
| 
| The option **-cfa** selects CFA mode, which generates 4 profiles: 1 for each CFA channel in a Bayer patterned image. This option cannot be used with color images or mono images with no Bayer pattern, and cannot be used at the same time as the **-tri** option.
| 
| The option **-arcsec** causes the x axis to display distance in arcsec, if the necessary metadata is available. This option is overridden if spectrometric options are provided. If not provided or if metadata is not available, distance will be shown in pixel units.
| 
| The argument **-savedat** will cause the GNUplot data files to be saved: the filename will be written to the log. Alternatively the argument **-filename=** can be used to specify a filename to write the data file to. (The **-filename=** option implies **-savedat**.)
| 
| **Spectrometric Options**
| 
| If spectrometric options are provided, all of the following must be provided: **-wavenumber1=** and **-wavenumber2=** specify 2 wavenumbers in cm\ :sup:`-1`, and **-wn1=x,y** and **-wn2=x,y** specify points in the image corresponding to those wavenumbers. As a convenience, **-wavelength1=** and **-wavelength2=** may be used instead to provide wavelengths in nm. These are always converted and the plot axis always shows wavenumber. The optional spectrometric argument **-width=** may be provided, which specifies how many pixels should be averaged perpendicular to the profile line.
| 
| The argument **"-title=\ My Title"** sets a custom title "My Title".

| Finds and optionally performs geometric transforms on images of the sequence given in argument so that they may be superimposed on the reference image. Using stars for registration, this algorithm only works with deep sky images. Star detection options can be changed using **SETFINDSTAR** or the *Dynamic PSF* dialog. The detection is done on the green layer for colour images, unless specified by the **-layer=** option with an argument ranging from 0 to 2 for red to blue.
| 
| The **-2pass** and **-noout** options will only compute the transforms but not generate the transformed images, **-2pass** adds a preliminary pass to the algorithm to find a good reference image before computing the transforms.\ **-nostarlist** disables saving the star lists to disk.
| 
| The option **-transf=** specifies the use of either **shift**, **similarity**, **affine** or **homography** (default) transformations respectively.
| The option **-drizzle** activates the sub-pixel stacking by up-scaling by 2 the generated images.
| The option **-minpairs=** will specify the minimum number of star pairs a frame must have with the reference frame, otherwise the frame will be dropped and excluded from the sequence.
| The option **-maxstars=** will specify the maximum number of star to find within each frame (must be between 100 and 2000). With more stars, a more accurate registration can be computed, but will take more time to run.
| 
| The pixel interpolation method can be specified with the **-interp=** argument followed by one of the methods in the list **no**\ [ne], **ne**\ [arest], **cu**\ [bic], **la**\ [nczos4], **li**\ [near], **ar**\ [ea]}. If **none** is passed, the transformation is forced to shift and a pixel-wise shift is applied to each image without any interpolation.
| Clamping of the bicubic and lanczos4 interpolation methods is the default, to avoid artefacts, but can be disabled with the **-noclamp** argument.
| 
| All images of the sequence will be registered unless the option **-selected** is passed, in that case the excluded images will not be processed
| 
| If created, the output sequence name starts with the prefix "r\_" unless otherwise specified with **-prefix=** option
| 
| Links: :ref:`setfindstar <setfindstar>`, :ref:`psf <psf>`

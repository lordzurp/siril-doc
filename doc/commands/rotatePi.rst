| Rotates the image of an angle of 180° around its center. This is equivalent to the command "ROTATE 180" or "ROTATE -180"
| 
| Links: :ref:`rotate <rotate>`

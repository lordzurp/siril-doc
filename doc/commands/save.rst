| Saves current image to **filename**.fit (or .fits, depending on your preferences, see SETEXT). Fits headers MIPS-HI and MIPS-LO are added with values corresponding to the current viewing levels
| 
| Links: :ref:`setext <setext>`

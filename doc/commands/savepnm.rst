| Saves current image under the form of a Netpbm file format with 16-bit per channel.
| 
| The extension of the output will be **filename**.ppm for RGB image and **filename**.pgm for gray-level image

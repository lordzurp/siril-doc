| Restores an image using the Split Bregman method.
| 
| Optionally, a PSF may be loaded using the argument **-loadpsf=\ filename**.
| 
| The number of iterations is provide by **-iters** (the default is 1).
| 
| The regularization factor **-alpha=** provides the regularization strength (lower value = more regularization, default = 3000)
| 
| Links: :ref:`psf <psf>`

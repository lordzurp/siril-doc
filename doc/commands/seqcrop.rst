| Crops the sequence given in argument **sequencename**.
| 
| The crop selection is specified by the upper left corner position **x** and **y** and the selection **width** and **height**.
| The output sequence name starts with the prefix "cropped\_" unless otherwise specified with **-prefix=** option

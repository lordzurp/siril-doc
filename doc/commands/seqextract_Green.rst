| Same command as EXTRACT_GREEN but for the sequence **sequencename**.
| 
| The output sequence name starts with the prefix "Green\_" unless otherwise specified with option **-prefix=**

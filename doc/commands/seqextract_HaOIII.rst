| Same command as EXTRACT_HAOIII but for the sequence **sequencename**.
| 
| The output sequences names start with the prefixes "Ha\_" and "OIII\_"
| 
| The optional argument **-resample={ha|oiii}** sets whether to upsample the Ha image or downsample the OIII image. If this argument is not provided, no resampling will be carried out and the OIII image will have twice the height and width of the Ha image

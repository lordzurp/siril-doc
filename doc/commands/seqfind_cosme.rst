| Same command as FIND_COSME but for the sequence **sequencename**.
| 
| The output sequence name starts with the prefix "cc\_" unless otherwise specified with **-prefix=** option
| 
| Links: :ref:`find_cosme <find_cosme>`

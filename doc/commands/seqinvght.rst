| Same command as INVGHT but the sequence must be specified as the first argument. In addition, the optional argument **-prefix=** can be used to set a custom prefix
| 
| Links: :ref:`invght <invght>`

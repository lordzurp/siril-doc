| Same command as MERGE_CFA but for the sequence **sequencename**.
| 
| The Bayer pattern to be reconstructed must be provided as the second argment as one of RGGB, BGGR, GBRG or GRBG.
| 
| The input filenames contain the identifying prefix "CFA\_" and a number unless otherwise specified with **-prefixin=** option.
| 
| Note: all 4 sets of input files **must** be present and **must** be consistently named, the only difference being the number after the identifying prefix.
| 
| The output sequence name starts with the prefix "mCFA\_" and a number unless otherwise specified with **-prefixout=** option
| 
| Links: :ref:`merge_cfa <merge_cfa>`

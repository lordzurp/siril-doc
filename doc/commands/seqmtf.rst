| Same command as MTF but for the sequence **sequencename**.
| 
| The output sequence name starts with the prefix "mtf\_" unless otherwise specified with **-prefix=** option.
| 
| Optionally the parameter **[channels]** may be used to specify the channels to apply the stretch to: this may be R, G, B, RG, RB or GB. The default is all channels
| 
| Links: :ref:`mtf <mtf>`

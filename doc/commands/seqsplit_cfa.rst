| Same command as SPLIT_CFA but for the sequence **sequencename**.
| 
| The output sequences names start with the prefix "CFA\_" and a number unless otherwise specified with **-prefix=** option
| 
| Links: :ref:`split_cfa <split_cfa>`

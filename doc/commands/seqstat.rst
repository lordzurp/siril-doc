| Same command as STAT for sequence **sequencename**.
| 
| The **output** is saved as a csv file given in second argument.
| The optional parameter defines the number of statistical values computed: **basic**, **main** (default) or **full** (more detailed but longer to compute).
| If **-cfa** is passed and the images are CFA, statistics are made on per-filter extractions
| 
| Links: :ref:`stat <stat>`

| Defines the number of processing threads used for calculation.
| 
| Can be as high as the number of virtual threads existing on the system, which is the number of CPU cores or twice this number if hyperthreading (Intel HT) is available

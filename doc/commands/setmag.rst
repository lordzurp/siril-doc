| Calibrates the magnitude by selecting a star and giving the known apparent magnitude.
| 
| All PSF computations will return the calibrated apparent magnitude afterwards, instead of an apparent magnitude relative to ADU values.
| To reset the magnitude constant see UNSETMAG
| 
| Links: :ref:`psf <psf>`, :ref:`unsetmag <unsetmag>`

| Search and display solar system objects in the current loaded and plate solved image. Use **-mag=** to change the limit magnitude which defaults to 20
| 
| This research has made use of IMCCE's `SkyBoT VO tool <https://ui.adsabs.harvard.edu/abs/2006ASPC..351..367B/abstract>`__

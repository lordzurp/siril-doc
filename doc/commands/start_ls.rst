| Initialize a livestacking session, using the optional calibration files and wait for input files to be provided by the LIVESTACK command until STOP_LS is called. Default processing will use shift-only registration and 16-bit processing because it's faster, it can be changed to rotation with **-rotate** and **-32bits**
| 
| Links: :ref:`livestack <livestack>`, :ref:`stop_ls <stop_ls>`

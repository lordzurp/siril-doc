| Stop the live stacking session. Only possible after START_LS
| 
| Links: :ref:`start_ls <start_ls>`

| Computes the wavelet transform on (**nbr_layers**\ =1...6) layer(s) using linear (**type**\ =1) or bspline (**type**\ =2) version of the 'à trous' algorithm. The result is stored in a file as a structure containing the layers, ready for weighted reconstruction with WRECONS
| 
| Links: :ref:`wrecons <wrecons>`

#########################
Installation on GNU/Linux
#########################

.. toctree::
   :hidden:

Installation of the AppImage binary
===================================

For GNU/Linux systems, we decided to provide a universal binary that works on 
GNU/Linux-like systems (x86_64). To run the AppImage binary, you just have to 
download it and allow it to run using the command:

.. code-block:: bash
      
      chmod +x Path/To/Application/Siril-x.y.z-x86_64.AppImage

By replacing with the correct path and x,y and z with the version numbers. Then
a simple double-click on the AppImage starts Siril.

Installation of the flatpak
===========================

Another way to install sable version of siril is to use the 
`flatpak <https://flathub.org/apps/details/org.free_astro.siril>`_, the utility
for software deployment and package management for Linux.
To install flatpak, type the following command:

.. code-block:: bash
      
      flatpak install flathub org.free_astro.siril

Then, to run the application:

.. code-block:: bash
      
      flatpak run org.free_astro.siril

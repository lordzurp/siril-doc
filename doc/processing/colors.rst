Colors
######

Color Calibration
=================

Siril offers two ways to retrieve the colors of your image.
Here, "retrieve" means re-balancing the RGB channels to get as close as 
possible to the true colors of the shot object.

Manual Color Calibration
************************

.. warning::
   The color calibration **must** be performed on a linear image whose 
   histogram has not yet been stretched. Otherwise, the colors obtained are not 
   guaranteed to be correct.
   
The manual way uses the following window:

.. figure:: ../_images/processing/color_manual_dialog.png
   :alt: dialog
   :class: with-shadow

   Manual Color Calibration dialog window.

The first step deals with the **background** of your image. The goal is to 
equalize the RGB layers in order the background appears as a neutral grey 
color.

After making a selection in your image (in a not so crowdy nor contrasted 
area), the area is taken into account by clicking on the :guilabel:`Use current
selection` button. The coordinates of the rectangle are displayed. Then 
:guilabel:`Background Neutralization` will calculate the median of each channel
and equalize them.

The second step deals with the **bright objetcs** of the picture. You can 
modify once again the histogram in two ways:

* Manually, with **White reference** and the 3 R, G and B coefficients, 
  according to your own taste. 
* Automatically, by selecting a rectangle area with contrasted objects (the 
  same way as previously)

Two sliders allow you to change the rejection limit for too dark and too bright
pixels in the selection.

As this is a trial and error process, you can undo the result with the 
:guilabel:`Undo` button (up left) and then try with other selections or 
coefficients until you are satisfied.

Photometric Color Calibration
*****************************

.. warning::
   The calibration of the colors by photometry **must** imperatively be carried
   out on a linear image whose histogram was not yet stretched. Without what, 
   the photometric measurements will be wrong and the colors obtained without 
   guarantee of being correct.

Another way for retrieving the colors is to compare the color of the stars in 
the image with their color in catalogues, to obtain the most natural color in
an automatic and non-subjective way. This is the PCC (Photometric Color
Calibration) tool. It can only work for images taken with a set of red, green
and blue filters for colors, or on-sensor color. To identify stars within the
image with those of the catalogue, an astrometric solution is required. Running
the PCC tool will first do that, so for this first part, please see the
:ref:`documentation of the plate solver module
<astrometry/platesolving:Platesolving>`.

.. note::

        This technique is heavily dependent on the type of filter used. Using
        different kinds of R, G, B filters will not make a large difference,
        but using a Light pollution filter or no IR-block filters will make the
        solution deviate significantly and not give the expected colors. 

Since version 1.2, the two tools run independently: it is possible to run the
photometric analysis and color correction of the image only if the image has
been already plate solved. It also means different catalogues can be used for
PCC and astrometry. And now, the tool is also available as the ``pcc`` command,
so it can be embedded in image post-processing scripts.

.. figure:: ../_images/processing/color_pcc_dialog.png
   :alt: dialog
   :class: with-shadow

   Photometric Color Calibration dialog window.

If the image was previously plate solved, turn on the
:ref:`annotations <astrometry/annotations:annotations>` feature to check that
catalogues align with the image. If the astrometric solution is not good
enough, checking :guilabel:`Force plate solving` will force its recomputation
as part of the PCC process.

As a reminder from the :ref:`plate solver documentation
<astrometry/platesolving:Platesolving>`, here is a summary of the options
visible in the window:

* Make sure the sampling is correct, computed from the focal length and pixel
  size found in the image or copied from the settings.

* The :guilabel:`Flip image if needed` allows to reorient the image correctly
  according to the astrometry result.

* For some oversampled or too large images, it is useful to check the 
  :guilabel:`Downsample image` to have more chances of success with the plate
  solving and it's also faster.

* The :guilabel:`Auto-crop (for wide field)` option will limit the field to 5 
  degrees in case you are dealing with very wide field images, mostly useful
  for plate solving.

* The **Catalog Settings** section allows you to choose which photometric 
  catalog should be used, NOMAD or APASS, as well as the limiting magnitude.

  .. tip::
     The NOMAD catalog can be :ref:`installed locally <astrometry/platesolving:Using local catalogues>`,
     while the APASS catalogue needs an internet access to get its content.

* The **Star Detection** section allows you to manually select which stars will
  be used for the photometry analysis. It's better to have hundreds of them at
  least, so individual picking would not be ideal.
  
  .. figure:: ../_images/processing/pcc_star_detect.png
     :alt: PCC star detection
     :class: with-shadow

* If desired, the **Background Reference** can be manually selected as 
  described in `Manual Color Calibration`_. This can be useful in the case of 
  nebula images where the background sky parts are small.

  .. figure:: ../_images/processing/pcc_bkg.png
     :alt: PCC background
     :class: with-shadow
 
When enough stars are found and the astrometric solution is correct, the PCC
will print this kind of text in the Console tab:

.. code-block:: text

        Applying aperture photometry to 433 stars.
        70 stars excluded from the calculation
        Distribution of errors: 1146 no error, 18 not in area, 48 inner radius too small, 4 pixel out of range
        Found a solution for color calibration using 363 stars. Factors:
        K0: 0.843	(deviation: 0.140)
        K1: 1.000	(deviation: 0.050)
        K2: 0.743	(deviation: 0.130)
        The photometric color correction seems to have found an imprecise solution, consider correcting the image gradient first

We can understand that 433 stars were selected from the catalogue and the image
for photometric analysis, but somehow, only 363 we actually used, 70 being
excluded. The line *Distribution of errors* explains for what reason they were
excluded: 18 were not found in the expected position, 48 were too big and 4
probably saturated. It is very common to have many stars rejected because they
don't meet the strict requirements for a valid photometric analysis.

We can also see that the PCC found three coefficients to apply to the color
channels to correct the white balance. The *deviation* here, which is the
average absolute deviation of the color correction for each of the star of the
photometric set, is moderately high. On well calibrated images without
gradient, with correct filters and without a color nebula covering the whole
image, devation would get closer to 0.

.. admonition:: Siril command line
   :class: sirilcommand

   .. include:: ../commands/pcc_use.rst

   .. include:: ../commands/pcc.rst


Color Saturation
================

This tool is used to increase the color saturation of the image. It is possible
to choose between a specific hue or the global hue to enhance. The strength of 
the saturation is adjusted with the slider :guilabel:`Amount`.

The :guilabel:`Background factor` slider sets the factor multiplied by the 
background value. Lower is the value, stronger is the saturation effect. While 
a high value will preserve the background.

.. figure:: ../_images/processing/color_saturation_dialog.png
   :alt: dialog
   :class: with-shadow

   Color Saturation dialog window.

.. admonition:: Siril command line
   :class: sirilcommand

   .. include:: ../commands/satu_use.rst

   .. include:: ../commands/satu.rst
   
Remove Green Noise
==================

Because green is not naturally present in deep sky images (except for comets 
and some planetary nebulae), if the image has already been calibrated, its 
colors are well balanced and the image is free of any gradient, we can assume 
that if the image contains green, it belongs to the noise. It is then 
interesting to find a method to remove this dominant green. This is exactly 
what the Remove Green Noise tool proposes, which is derived from the 
Subtractive Color Noise Reduction tool, but for green only.

.. figure:: ../_images/processing/scnr.png
   :alt: dialog
   :class: with-shadow

   Remove Green Noise dialog window.
   
This tool has 3 settings. The protection method, the amount (called :math:`a`
in the following section), and a :guilabel:`Preserve lightness` button. The 
following methods present the different existing ways to remove the green 
pixels by replacing them with a mix of Red and Blue. The amount is only 
available for methods with mask protection. The choice of its value must be 
done with caution in order to minimize the rise of the magenta cast in the sky 
background. Do not hesitate to use the :guilabel:`Undo` and :guilabel:`Redo` 
buttons in order to fine-tune the value.
   
Protection method
*****************

.. rubric:: Maximum Mask Protection

.. math::

   m &= \text{max}(R,B) \\
   G'&= G\times (1 — a)\times (1 — m) + m\times G

.. rubric:: Additive Mask Protection

.. math::
  
   m &= \text{min}(1,R+B) \\
   G'&= G\times (1 — a)\times (1 — m) + m\times G

.. rubric:: Average Neutral Protection (default method)

.. math::
  
   m &= 0.5\times (R + B) \\
   G'&= \text{min}(G, m)

.. rubric:: Maximum Neutral Protection

.. math::
  
   m &= \text{max}(R,B) \\
   G'&= \text{min}(G, m)
   
Finally, the :guilabel:`Preserve lightness` button preserves the original CIE L* 
component in the processed image, in order to only process chromatic component, 
it is highly recommended to let this option checked.

.. admonition:: Siril command line
   :class: sirilcommand
   
   .. include:: ../commands/rmgreen_use.rst

   .. include:: ../commands/rmgreen.rst
   
Negative Transform
==================

.. |negative-icon| image:: ../_images/icons/neg.svg

Negative transformation refers to subtracting pixel values from :math:`(L−1)`,
where :math:`L` is the maximum possible value of the pixel, and replacing it 
with the result.

The :guilabel:`Negative transformation` tool is different from the negative 
view |negative-icon| in the toolbar. Indeed, the transformation is not 
only visual, but actually applied to the pixel values. If you save the image, 
it will be saved as a negative.

.. figure:: ../_images/processing/negative_or.png
   :alt: dialog
   :class: with-shadow
   :width: 100%

   Original image with weak signal (Image Cyril Richard).
   
.. figure:: ../_images/processing/negative_neg.png
   :alt: dialog
   :class: with-shadow
   :width: 100%

   Negative image where the signal is more visible (Image Cyril Richard).
   
.. tip::
   A common use of the negative transformation tool is to remove the magenta 
   cast from SHO images. In this case one need to apply :guilabel:`Negative transformation`,
   then :guilabel:`Remove Green Noise`, then :guilabel:`Negative transformation`
   again.
   
.. admonition:: Siril command line
   :class: sirilcommand
   
   .. include:: ../commands/neg_use.rst

   .. include:: ../commands/neg.rst


Filters
=======

This section presents all the filters present in Siril. Filters are tools that 
will modify the pixels of the image according to the needs.


.. toctree::
   :hidden:

   atrouwavelets
   banding
   clahe
   cc
   deconvolution
   ft
   median
   denoising
   rgradient

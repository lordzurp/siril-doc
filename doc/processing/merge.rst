Merge CFA Channels
==================

.. figure:: ../_images/processing/merge_dialog.png
   :alt: Merge Elements dialog
   :class: with-shadow
   
The purpose of this tool is to combine multiple monochrome images that have 
been previously extracted from a CFA sensor (with the 
:menuselection:`Extraction --> Split CFA channels...` menu for example). The 
tool merges the separate red, green (x2), and blue channel images into a single 
composite image called CFA image.

.. warning::
   This tool is dedicated to images from a Bayer matrix and therefore it cannot
   work with images from X-Trans files from Fuji cameras.

The dialog is split in three different parts:

* **Input files**: Select the image containing the CFA0, CFA1, CFA2 and CFA3 
  Bayer subpatterns. If this has been produced using Siril's Split CFA function 
  it will have the CFA prefix.

* **Bayer Pattern**: Sets the Bayer pattern header to be applied to the result.
  This must match the Bayer pattern of the image that the original Bayer 
  subchannels were split from.

* The sequence part, at the bottom, allows to process whole sequences by 
  reconstituting a CFA image sequence. Clicking on the :guilabel:`Apply to 
  sequence` button displays a help text to proceed correctly. This text is 
  reported in the next tooltip. There are two available options:
  
    - **Sequence input marker**: Identifier prefix used to denote CFA number 
      in the separate CFA channel images. This should be set to whatever sequence
      prefix was used when the split_cfa process was run (default: ``CFA_``).
    - **Sequence output prefix**: Prefix of the image names resulting from the 
      merge CFA process. By default it is ``mCFA_``.
  
  .. tip::
     You must have the CFA0 sequence selected in the main window sequence tab.

     Your separate sub-CFA sequences must have been processed in exactly the 
     same way.

     The filenames **must** be in the same directory and **must** differ only 
     by the name of the CFA channel. i.e. if a CFA0 image is 
     *r_pp_CFA_0_Light_0001.fit*, the corresponding images for the other CFA 
     channels must be *r_pp_CFA_1_Light_0001.fit*, *r_pp_CFA_2_Light_0001.fit* 
     and *r_pp_CFA_3_Light_0001.fit*.
     
     Each image in the sequence will only be processed if the corresponding 
     images for the other 3 CFA channels can be found. Both G1 and G2 are 
     required. Note this means that if you discard an image containing one CFA 
     channel of an image between split_cfa and merge_cfa, merge_cfa will be 
     unable to merge the remaining CFA channels for that image. All sequence 
     filtering should be done either before split_cfa or after merge_cfa.


  
.. admonition:: Siril command line
   :class: sirilcommand
   
   .. include:: ../commands/merge_cfa_use.rst

   .. include:: ../commands/merge_cfa.rst
     
.. admonition:: Siril command line
   :class: sirilcommand
   
   .. include:: ../commands/seqmerge_cfa_use.rst

   .. include:: ../commands/seqmerge_cfa.rst

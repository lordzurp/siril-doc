from subprocess import Popen, PIPE, STDOUT
import os, sys
import re

if sys.platform.startswith('win32'):
    siril = R"C:\msys64\mingw64\bin\siril-cli.exe -s -"
else:
    siril = "siril -s -"

p = Popen(siril, stdout=PIPE, stdin=PIPE, stderr=STDOUT)
log = p.communicate(input=b'requires 1.1.0\nget -A')[0].decode()
lines = list(line[5:] for line in (l.strip() for l in log.split('\n')) if line and line.startswith('log:'))
flag = False
out = []
pattern = r' \(([^)]+)\), ([^)]+)'
for l in lines:
    if not flag and not l.startswith('Running command: get'):
        continue
    if l.startswith('Running command: get'):
        flag = True
        continue
    try:
        a, b = l.split(' = ', 1)
        result = re.search(pattern, b)
        c, d = result.groups()
        b = b[:result.span()[0]]
        out += ['{:s}\t{:s}\t{:s}\t{:s}'.format(a,b,c,d)]
    except:
        pass

with open('./doc/preferences/getA.txt','wb') as f:
    f.write('Variable\tDefault ([Range])\tType\tComment\n'.encode('utf8'))
    f.write('\n'.join(out).encode('utf8'))